﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Text scoreText;
    static int highScore;

    void Start() 
    {
        highScore = PlayerPrefs.GetInt("HS");
    }

    void Update()
    {
        scoreText.text = "" + highScore;      
        PlayerPrefs.SetInt("HS2", highScore);
        Debug.Log("HighScore :" + highScore);

    }
    public void playGame()
    {
        SceneManager.LoadScene("LoadingSceneMtoG");
    }

    public void quitGame()
    {
        Application.Quit();
    }
    
}
