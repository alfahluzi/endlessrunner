﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioController : MonoBehaviour
{
    private AudioSource audioSource;
    public Slider bgmSlider, sfxSlider;
    private static float bgmVolume = 0.7f;
    private static float sfxVolume = 0.7f;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {
        audioSource.volume = bgmVolume;
        bgmSlider.value = bgmVolume;
        sfxSlider.value = sfxVolume;
    }

    public void setVolBGM(float vol)
    {
        bgmVolume = vol;
        PlayerPrefs.SetFloat("BGM", bgmVolume);
        bgmSlider.value = vol;
    }

    public void setVolSFX(float vol)
    {
        sfxVolume = vol;
        PlayerPrefs.SetFloat("SFX", sfxVolume);
        sfxSlider.value = vol;
    }
}
