﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    float time = 0;
    public GameObject Spawner;
    public float timer;
    public float deceresTimer;
    public float minTime;

    public float kecepatan;
    public float increasKecepatan;
    public float maxKecepatan;
    float updateKecepatan = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (time <= 0)
        {
            
            time = kecepatan;                
            if (kecepatan < maxKecepatan)
            {
                kecepatan +=increasKecepatan;

                if (kecepatan%10 == 0)
                {
                    updateKecepatan = 5*(kecepatan/10);    
                }
                PlayerPrefs.SetFloat("vObs", updateKecepatan);
            }            

            Instantiate(Spawner, transform.position, Quaternion.identity);            
            time = timer;
            if (timer > minTime)
            {
                timer -=deceresTimer;
            }
        }
        else
        {
            time -= Time.deltaTime;
        }

        //#####Ngatur kecepatan dan lfe time###########
        
        //############################################
    }
}
