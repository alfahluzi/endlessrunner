﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AudioControl : MonoBehaviour
{
    private AudioSource audioSource;
    private static float bgmVolume = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        bgmVolume = PlayerPrefs.GetFloat("BGM");
        audioSource.volume = bgmVolume;
    }

}
