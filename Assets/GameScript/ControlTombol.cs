﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlTombol : MonoBehaviour
{
    public GameObject Player;
    public GameObject slashEffect;
    //public GameObject Particle;

    public LayerMask whatIsGround;
    //public LayerMask whatsIsEnemy;
    public Transform groundCheck;
    private Rigidbody2D Rb;
    private Animator anim;
    public Transform slashPoint;

    private int extraJumps;
    private bool isGrounded;
    private float ScreenWidth;
    public int extraJumpsValue;
    public float jumpForce;
    public float checkRadius;
    private float timeBtwAtk;
    public float startTimeBtwAtk;
    // Start is called before the first frame update
    void Start()
    {
        Rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        extraJumps = extraJumpsValue;
    }
    void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
    }
    // Update is called once per frame
    void Update()
    {
        if (isGrounded == true)
        {
            extraJumps = extraJumpsValue;
        }
        //Jump&Attack coding
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (mousePosition.x < ScreenWidth / 2)
            {
                if (extraJumps > 0)
                {
                    Rb.velocity = Vector2.up * jumpForce;
                    SFXAudioScript.PlaySound("jump");
                    extraJumps--;
                }
                else if (extraJumps == 0 && isGrounded == true)
                {
                    Rb.velocity = Vector2.up * jumpForce;
                    SFXAudioScript.PlaySound("jump");
                    extraJumps = extraJumpsValue;
                }
            }


            if (mousePosition.x > ScreenWidth / 2)
            {
                if (timeBtwAtk <= 0)
                {
                    timeBtwAtk = startTimeBtwAtk;
                    anim.SetTrigger("Slash");
                    SFXAudioScript.PlaySound("slash");
                    Instantiate(slashEffect, slashPoint.position, transform.rotation);
                }
            }
        }
        if (timeBtwAtk > 0)
        {
            timeBtwAtk -= Time.deltaTime;
        }
    }
}
