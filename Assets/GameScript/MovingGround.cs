﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingGround : MonoBehaviour
{
    float time = 0;
    public float timer;
    public float inceresTimer;
    public float maxTime;
    public float setTimer;
    public float multipleOfTimer;
    // Update is called once per frame
    void Start()
    {

    }
    void Update()
    {   
        if (time <= 0)
        {   
            time = timer;
            if (timer < maxTime)
            {
                timer +=inceresTimer;
            }
            if (timer%multipleOfTimer == 0)
            {
                setTimer = timer;
            }
            
        }
        else
        {
            time -= Time.deltaTime;
        }
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(-1000000.0f, -4.479516f), Time.deltaTime*setTimer);
    }
}
