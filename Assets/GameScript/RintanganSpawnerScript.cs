﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RintanganSpawnerScript : MonoBehaviour
{
    float time = 0;
    public GameObject Rintangan;
    public float timer;
    void Update()
    {
        if (time <= 0)
        {
            Instantiate(Rintangan, transform.position, Quaternion.identity);
            time = timer;
        }
        else
        {
            time -= Time.deltaTime;
        }
    }
}
