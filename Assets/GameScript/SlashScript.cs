﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SlashScript : MonoBehaviour
{
    public float speed;
    public float distance;
    public LayerMask whatsIsEnemy;
    public float lifeTime;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroySlashEffect", lifeTime);
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, transform.up, distance, whatsIsEnemy);
        if(hitInfo.collider != null)
        {
            if (hitInfo.collider.CompareTag("Obstacle"))
            {
                hitInfo.collider.GetComponent<Obstacle>().TakeDamage();
            }
        }
        transform.Translate(transform.right*speed*Time.deltaTime);                         
    }

    void DestroySlashEffect()
    {
        Destroy(gameObject);
    }
}
