﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Obstacle : MonoBehaviour
{
    float nilaiRandom;
    public GameObject Particle;
    float kecepatan = 5;
    public float ymin, ymax;
    // Start is called before the first frame update
    void Start()
    {
        nilaiRandom = Random.Range(ymin, ymax);
        transform.position = new Vector2(transform.position.x, nilaiRandom);
        kecepatan = PlayerPrefs.GetFloat("vObs");
        
        Invoke("TakeDamage", 50);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Kecepatan :"+kecepatan);
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(-100.0f, nilaiRandom), Time.deltaTime *kecepatan);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Destroy(gameObject);
            SceneManager.LoadScene("LoadingSceneGtoM");
        }
    }


    public void TakeDamage()
    {
        Destroy(gameObject);
        Instantiate(Particle, transform.position, Quaternion.identity);
    }
}
