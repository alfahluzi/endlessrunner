﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXAudioScript : MonoBehaviour
{
    public static AudioClip runSound, jumpsSound, slashSound, obsDestSound;
    static AudioSource audioSrc;
    private static float sfxVolume = 1.0f;

    void Start()
    {
        jumpsSound = Resources.Load<AudioClip>("Audio/SFX/jump1");
        slashSound = Resources.Load<AudioClip>("Audio/SFX/attack3");
        //obsDestSound = Resources.Load<AudioClip>("");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        sfxVolume = PlayerPrefs.GetFloat("SFX");
        audioSrc.volume = sfxVolume;
    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "jump":audioSrc.PlayOneShot(jumpsSound);break;
            case "slash":audioSrc.PlayOneShot(slashSound);break;
            //case "obsDest":audioSrc.PlayOneShot(obsDestSound);break;
        }
    }
}