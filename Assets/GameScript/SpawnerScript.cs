﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour
{
    float time = 0;
    public GameObject TriggerScore;
    public float timer;
 
    // Update is called once per frame
    void Update()
    {
        if (time <= 0)
        {
            Instantiate(TriggerScore, transform.position, Quaternion.identity);
            time = timer;
        }
        else
        {
            time -= Time.deltaTime;
        }
    }
}
