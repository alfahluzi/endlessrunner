﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{   
    public int score;
    static int highScore;
    public Text scoreTxt;
    public Text highScoreTxt;

    void Start()
    {
        highScore = PlayerPrefs.GetInt("HS2");
        scoreTxt.text = "Score " + score;
        highScoreTxt.text = "High score " + highScore;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "point")
        {
            score++;
        }
    }


    void Update()
    {
        scoreTxt.text = "Score " + score;

        if (highScore < score)
        {
            highScore = score;
            highScoreTxt.text = "High score " + highScore;
        }
        PlayerPrefs.SetInt("HS", highScore);
    }

    
}
