﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawnerScript : MonoBehaviour
{
    public GameObject coinObj;
    float nilaiRandom;
    public float from;
    public float to;
    void Start()
    {
        InvokeRepeating("Spawn", from, to);
    }

    void Update()
    {
        //nilaiRandom = Random.Range(5.0f, 8.0f);
        
    }

    void Spawn()
    {
        Instantiate(coinObj, transform.position, Quaternion.identity);
    }
}
