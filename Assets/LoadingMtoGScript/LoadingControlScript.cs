﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingControlScript : MonoBehaviour
{   
    public Slider slider; 
    AsyncOperation async;
    void Start()
    {
        StartCoroutine(LoadingScreen());
    }

    void Update()
    {
        
    }

    IEnumerator LoadingScreen()
    {
        async = SceneManager.LoadSceneAsync("Game");
        async.allowSceneActivation = false ;
        while (async.isDone == false)
        {
            slider.value = async.progress;
            if (async.progress == 0.9f)  
            {
                slider.value = 1f;
                async.allowSceneActivation = true;
            }
            yield return null;
        }
    }
}
